#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np

class Elementarzelle:
    def __init__(self, seiten, winkel): 
        self.a, self.b, self.c = seiten
        self.alpha, self.beta, self.gamma = winkel
        
    def calc_volume(self):
        volume = self.a * self.b * self.c * np.sqrt(1 - np.cos(self.alpha)**2 - 
                 np.cos(self.beta)**2 - np.cos(self.gamma)**2 +
                 2 * np.cos(self.alpha) * np.cos(self.beta) * np.cos(self.gamma))
        return volume
        
class Koordinaten:
    def __init__(self, coord):
        self.x, self.y, self.z = coord

def distance(a1_coord, a2_coord, ez):
    delta_x = a1_coord.x - a2_coord.x
    delta_y = a1_coord.y - a2_coord.y
    delta_z = a1_coord.z - a2_coord.z
    d = np.sqrt((ez.a * delta_x)**2 + (ez.b * delta_y)**2 + (ez.c * delta_z)**2 + \
        2 * ez.b * ez.c * (delta_y * delta_z) * np.cos(ez.alpha) + \
        2 * ez.a * ez.c * (delta_x * delta_z) * np.cos(ez.beta)  + \
        2 * ez.a * ez.b * (delta_x * delta_y) * np.cos(ez.gamma)) 
    return d

#Eingabe der Seitenlängen a, b, c
abc = input('Seitenlängen a b c in Å; z.B. 2 3 4 oder "Enter" für 2 3 4: ')

if abc == '':
    seiten = (2, 3, 4)
else:
    seiten = (float(s) for s in abc.split())
    
print('')

#Eingabe der Winkel alpha, beta, gamma
al_be_ga = input('Winkel alpha beta gamma in °; z.B. 90 90 90 oder "Enter" für 90 90 90: ')

if al_be_ga  == '':
    winkel = (np.radians(90), np.radians(90), np.radians(90))
else:
    winkel = (np.radians(float(w)) for w in al_be_ga.split())
    
#Definition der EZ über a, b, c und alpha, beta, gamma
ez = Elementarzelle(seiten, winkel)

print('')

#Eingabe der Koordinaten für Atom 1
a1 = input('Koordinaten x y z für Atom 1; z.B. 0.1 0.2 0.3 oder "Enter" für 0 0 0: ')

if a1  == '':
    a1_coord = Koordinaten((0, 0, 0))
else:
    a1_coord = Koordinaten(float(c) for c in a1.split())

print('')

#Eingabe der Koordinaten für Atom 2
a2 = input('Koordinaten x y z für Atom 2; z.B. 0.1 0.2 0.3 oder "Enter" für 1 1 1: ')

if a2  == '':
    a2_coord = Koordinaten((1, 1, 1))
else:
    a2_coord = Koordinaten(float(c) for c in a2.split())

print('')

#Berechnung des Abstands der Atome 1 und 2
abstand = distance(a1_coord, a2_coord, ez)
#Berechnung des Volumens der Elementarzelle
volumen = ez.calc_volume()

#Ausgabe der Ergebnisse
print('Das Volumen der Elementarzelle beträgt', '{:.3f}'.format(volumen), 'Å³.')
print('Der Abstand der Atome 1 und 2 beträgt', '{:.3f}'.format(abstand), 'Å.')

